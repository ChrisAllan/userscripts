// ==UserScript==
// @name         Youtube playlist mods
// @namespace    chrisallan
// @version      0.5
// @description  Adds button to clear a playlist, option to hide watched videos, and direct links to videos.
// @author       Chris Allan
// @match        https://*.youtube.com/*
// @grant        none
// ==/UserScript==

(function() {
	document.addEventListener("spfdone", checkPage);
	checkPage();
})();

function checkPage() {
	if (location.pathname == '/playlist') {
		addClearPlaylistButton();
		makeDirectLinks();
		addHideWatchedSwich();
	}
}

// Add "Clear Playlist" button.
function addClearPlaylistButton() {
	var button_text = '<span class="yt-uix-button-content">Clear Playlist</span>';

	var container = document.getElementsByClassName('playlist-auxiliary-actions')[0];

	if (container.children.length > 0) {
		var button = document.createElement('button');
		button.className = 'yt-uix-button yt-uix-button-size-default yt-uix-button-default';
		button.type = 'button';
		button.innerHTML = button_text;
		button.onclick = clearAll;

		container.children[0].style.display = 'inline-block';
		container.appendChild(button);
	}
}

// Make links direct-video links
function makeDirectLinks() {
	loadAll(function() {
		var links = document.getElementsByClassName('pl-video-title-link');
		for (i = 0; i < links.length; ++i) {
			var href = links[i].href;
			links[i].href = href.slice(0, href.indexOf('&'));
		}
	});
}

// Toggle hide watched
function addHideWatchedSwich() {
	var header = document.getElementsByClassName('pl-header-content')[0];
	var toggleHide = document.createElement('div');
	var active = false;
	var ignore = false;

	toggleHide.innerHTML = '<label id="hideWatchedContainer"><span class="yt-uix-form-input-checkbox-container"><input type="checkbox" id="hideWatched" /><span class="yt-uix-form-input-checkbox-element"></span></span> Hide watched</label>';
	toggleHide.style = 'padding-top: 1em;';
	toggleHide.onclick = function() {
		ignore = !ignore;
		if (ignore) return;
		active = !active;
		if (active) {
			hideWatched();
		}
		else {
			showWatched();
		}
	};

	header.appendChild(toggleHide);
}

function hideWatched() {
	loadAll(function() {
		var watched = document.getElementsByClassName('resume-playback-background');
		for (i = 0; i < watched.length; ++i) {
			watched[i].parentElement.parentElement.parentElement.style.display = 'none';
		}
	});
}

function showWatched() {
	loadAll(function() {
		var watched = document.getElementsByClassName('resume-playback-background');
		for (i = 0; i < watched.length; ++i) {
			watched[i].parentElement.parentElement.parentElement.style.display = 'table-row';
		}
	});
}

function loadAll(_callback) {
	var loadInterval = setInterval(_load, 100);

	function _load() {
		LoadMoreButton = document.getElementsByClassName('load-more-button');
		if (LoadMoreButton.length > 0) {
			LoadMoreButton[0].click();
		}
		else {
			clearInterval(loadInterval);
			_callback();
		}
	}
}

function clearAll() {
	var IntervalCDB;

	loadAll(function() {
		IntervalCDB = setInterval(_clickDelButton, 1);
	});

	function _clickDelButton() {
		DelButton = document.getElementsByClassName('pl-video-edit-remove');
		DelButton[0].click();
		if (DelButton.length == 1) {
			clearInterval(IntervalCDB);
		}
	}
}

